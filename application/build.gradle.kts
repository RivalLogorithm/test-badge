plugins {
    id("myproject.java-conventions")
    id("jacoco-report-aggregation") // <1>
}

dependencies {
    implementation(project(":list"))
    implementation(project(":utilities"))
}


tasks.check {
    finalizedBy(tasks.named<JacocoReport>("testCodeCoverageReport"))
}

tasks.jar {
    enabled = true
}
